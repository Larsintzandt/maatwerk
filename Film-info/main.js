$(document).ready(function() {
    $('#searchForm').on('submit', function(e) {
        let searchText = $('#searchText').val();
        getMovies(searchText);
        e.preventDefault();
    });
});

function getMovies(searchText){
    axios.get('http://www.omdbapi.com/?i=tt3896198&apikey=634ee8bd&s='+searchText)
    .then(function(response) {
        console.log(response);
        let movies = response.data.Search;
        let output = '';
        $.each(movies, function(index, movie) {
            output += `
                <div class="col-md-3">
                    <div class="well text-center">
                        <h5>${movie.Title}</h5>
                        <img src="${movie.Poster}">
                        <a onclick="movieSelected('${movie.imdbID}')" class="btn btn-primary" href="#">Extra Film Details</a>
                    </div>
                </div>
            `;
        });

        $('#movies').html(output);
    })
    .catch(function(err) {
        console.log(err);
    })
}

function movieSelected(id) {
    sessionStorage.setItem('movieId', id);
    window.location = 'film.html';
    return false;
}

function getMovie(){
    let movieId = sessionStorage.getItem('movieId');

    
}