let counter = document.querySelector('.counter');
const addCount = document.querySelector('#addCountBtn');
const lowerCount = document.querySelector('#lowerCountBtn');

let count = 0;

addCount.addEventListener('click',plusCounter);
lowerCount.addEventListener('click',minCounter);

function plusCounter (){
    count++;
    counter.innerHTML = count;
    if(counter.innerHTML > '0'){
        counter.style.color = 'chartreuse'
    }
    else if(counter.innerHTML === '0'){
        counter.style.color = 'black';
    }
}

function minCounter (){
    count--;
    counter.innerHTML = count;
     if(counter.innerHTML < '0'){
        counter.style.color = 'red'
    }
    else if(counter.innerHTML === '0'){
        counter.style.color = 'black';
    }
}